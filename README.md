### Jira Directory Configuration Formatter ###

Tired of trying to read the directoryConfigurationSummary.txt? Having a hard time remembering the difference between ldap.user.dn and ldap.userdn? This is the tool for you! Pass a directoryConfigurationSummary.txt to the script and it will output the data
in the order that it appears in Jira, with optional Jira markup mode for embedding into a Jira ticket.

### How do I get set up? ###

- Install Python, if for some reason it is not already installed.

### How do I use the script? ###

First argument is the input file, with an optional flag -j if you want Jira markup output to embed in a support case. 

Example:
```
python betterUserDirectory.py directoryConfigurationSummary.txt -j
```
