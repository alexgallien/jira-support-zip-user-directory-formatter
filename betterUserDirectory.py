import argparse
import time

# -----------------------------------------------------------------------------
'''
Usage: Parses a directoryConfigurationSummary.txt into something more human readable.
First argument is the input file, with an optional flag -j if you want Jira markup 
output to embed in a support case.

e.x. python betterUserDirectory.py multiples.txt -j
'''

# -----------------------------------------------------------------------------
'''
Todo (! for things done in this version)

'''

# -----------------------------------------------------------------------------


class BetterUserDirectory(object):
    def __init__(self, content, jira):
        self.content = content
        self.jira = jira

    def processor(self, content):
        start = 0
        counter = 0
        working_chunk = {}
        chunks = []

        for line in content:
            if line == "=============================" and start == 1:
                # End the proessing of this chunk.
                start = 0
                chunks.append(working_chunk)
                counter += 1
                working_chunk = {}
            elif line == "=============================":
                # Start creating a new entry in the dictionary.
                start = 1
            elif not line.strip():
                # Checking for empty lines.
                continue
            elif start == 0 and line != "=============================" and line != "":
                # Getting the name of the directory.
                working_chunk.update({"Directory Name": line})
            else:
                lsplit = line.split(': ')
                working_chunk.update({lsplit[0]: lsplit[1]})
        return chunks

    def text_printer(self, chunk):
        print('## Server Settings ##\n')

        if 'Directory Name' in chunk: print('Name: ' + chunk['Directory Name'])
        if 'Directory ID' in chunk: print('Directory ID: ' + chunk['Directory ID'])
        if 'Active' in chunk: print('Active: ' + chunk['Active'])
        if 'Directory Type' in chunk: print('Directory Type: ' + chunk['Directory Type'])
        if 'crowd.delegated.directory.type' in chunk: print(
        'Delegated Directory Type: ' + chunk['crowd.delegated.directory.type'])
        if 'ldap.url' in chunk: print('Hostname: ' + chunk['ldap.url'])
        if 'crowd.server.url' in chunk: print('Crowd Server URL: ' + chunk['crowd.server.url'])
        if 'application.name' in chunk: print('Application Name: ' + chunk['application.name'])
        if 'user_encryption_method' in chunk: print('Encryption Method: ' + chunk['user_encryption_method'])
        if 'ldap.userdn' in chunk: print('Username: ' + chunk['ldap.userdn'])
        if 'ldap.password' in chunk: print('Password: ' + chunk['ldap.password'])
        if 'autoAddGroups' in chunk: print('Default Group Memberships: ' + chunk['autoAddGroups'])
        if 'crowd.delegated.directory.auto.create.user' in chunk: print('Automatically Create User On Login: ' + chunk['crowd.delegated.directory.auto.create.user'])
        if 'crowd.delegated.directory.auto.update.user' in chunk: print('Automatically Update User On Login: ' + chunk['crowd.delegated.directory.auto.update.user'])
        if 'crowd.delegated.directory.importGroups' in chunk: print(
        'Synchronize Group Members: ' + chunk['crowd.delegated.directory.importGroups'])

        if 'ldap.basedn' in chunk: print('\n## LDAP Schema ##\n')

        if 'ldap.basedn' in chunk: print('Base DN: ' + chunk['ldap.basedn'])
        if 'ldap.user.dn' in chunk: print('Additional User DN: ' + chunk['ldap.user.dn'])
        if 'ldap.group.dn' in chunk: print('Additional Group DN: ' + chunk['ldap.group.dn'])
        if 'ldap.local.groups' in chunk: print('Read Only, with Local Groups: ' + chunk['ldap.local.groups'])

        if 'com.atlassian.crowd.directory.sync.laststartsynctime' in chunk: print('\n## Synchronization Information ##\n')

        if 'com.atlassian.crowd.directory.sync.issynchronising' in chunk: print('Currently Synchronizing: ' + chunk['com.atlassian.crowd.directory.sync.issynchronising'])
        if 'com.atlassian.crowd.directory.sync.currentstartsynctime' in chunk: print(
        'Current Sync Start Time: ' + chunk['com.atlassian.crowd.directory.sync.currentstartsynctime'])
        if 'com.atlassian.crowd.directory.sync.laststartsynctime' in chunk: print('Last Sync Start Time: ' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(chunk['com.atlassian.crowd.directory.sync.laststartsynctime'])/1000)))
        if 'com.atlassian.crowd.directory.sync.lastdurationms' in chunk: print(
        'Last Sync Duration (ms): ' + chunk['com.atlassian.crowd.directory.sync.lastdurationms'])

        print('\n## Advanced Settings ##\n')

        if 'ldap.nestedgroups.disabled' in chunk: print('Disable Nested Groups: ' + chunk['ldap.nestedgroups.disabled'])
        if 'localUserStatusEnabled' in chunk: print('Manage User Status Locally: ' + chunk['localUserStatusEnabled'])
        if 'ldap.filter.expiredUsers' in chunk: print('Filter out expired Users: ' + chunk['ldap.filter.expiredUsers'])
        if 'ldap.pagedresults' in chunk: print('Use Paged Results: ' + chunk['ldap.pagedresults'])
        if 'ldap.pagedresults.size' in chunk: print('Number of Paged Results: ' + chunk['ldap.pagedresults.size'])
        if 'ldap.referral' in chunk: print('Follow Referrals: ' + chunk['ldap.referral'])
        if 'ldap.relaxed.dn.standardisation' in chunk: print(
        'Native DN Matching: ' + chunk['ldap.relaxed.dn.standardisation'])
        if 'crowd.sync.incremental.enabled' in chunk: print(
        'Incremental Synchronization: ' + chunk['crowd.sync.incremental.enabled'])
        if 'crowd.sync.group.membership.after.successful.user.auth.enabled' in chunk: print(
        'Update group memberships when logging in: ' + chunk[
            'crowd.sync.group.membership.after.successful.user.auth.enabled'])
        if 'directory.cache.synchronise.interval' in chunk: print(
        'Synchronization Interval: ' + chunk['directory.cache.synchronise.interval'])
        if 'ldap.read.timeout' in chunk: print('Read timeout: ' + chunk['ldap.read.timeout'])
        if 'ldap.search.timelimit' in chunk: print('Search Timeout: ' + chunk['ldap.search.timelimit'])
        if 'ldap.connection.timeout' in chunk: print('Connection Timeout: ' + chunk['ldap.connection.timeout'])

        if 'ldap.user.filter' in chunk: print('\n## User Schema Settings ##\n')

        if 'ldap.user.objectclass' in chunk: print('User Object Class: ' + chunk['ldap.user.objectclass'])
        if 'ldap.user.filter' in chunk: print('User Object Filter: ' + chunk['ldap.user.filter'])
        if 'ldap.user.username' in chunk: print('User Name Attribute: ' + chunk['ldap.user.username'])
        if 'ldap.user.username.rdn' in chunk: print('User Name RDN Attribute: ' + chunk['ldap.user.username.rdn'])
        if 'ldap.user.firstname' in chunk: print('User First Name Attribute: ' + chunk['ldap.user.firstname'])
        if 'ldap.user.lastname' in chunk: print('User Last Name Attribute: ' + chunk['ldap.user.lastname'])
        if 'ldap.user.displayname' in chunk: print('User Display Name Attribute: ' + chunk['ldap.user.displayname'])
        if 'ldap.user.email' in chunk: print('User Email Attribute: ' + chunk['ldap.user.email'])
        if 'ldap.user.password' in chunk: print('User Password Attribute: ' + chunk['ldap.user.password'])
        if 'ldap.user.external.id' in chunk: print('User Unique ID Attribute: ' + chunk['ldap.user.external.id'])

        if 'ldap.group.filter' in chunk: print('\n## Group Schema Settings ##\n')

        if 'ldap.group.objectclass' in chunk: print('Group Object Class: ' + chunk['ldap.group.objectclass'])
        if 'ldap.group.filter' in chunk: print('Group Object Filter: ' + chunk['ldap.group.filter'])
        if 'ldap.group.name' in chunk: print('Group Name Attribute: ' + chunk['ldap.group.name'])
        if 'ldap.group.description' in chunk: print('Group Description Attribute: ' + chunk['ldap.group.description'])

        if 'ldap.user.group' in chunk: print('\n## Membership Schema Settings ##\n')

        if 'ldap.group.usernames' in chunk: print('Group Members Attribute: ' + chunk['ldap.group.usernames'])
        if 'ldap.user.group' in chunk: print('User Membership Attribute: ' + chunk['ldap.user.group'])
        if 'ldap.usermembership.use' in chunk: print(
        'When finding the user\'s group membership: ' + chunk['ldap.usermembership.use'])
        if 'ldap.usermembership.use.for.groups' in chunk: print(
        'When finding the members of a group: ' + chunk['ldap.usermembership.use.for.groups'])

    def jira_printer(self, chunk):
        print('{panel:title=Directory name: ' + chunk[
            'Directory Name'] + '|borderStyle=solid|borderColor=#ccc|titleBGColor=#3572b0|titleColor=#FFFFFF}')
        print('h4. Server Settings\n')

        if 'Directory Name' in chunk: print('*Name:* ' + chunk['Directory Name'])
        if 'Directory ID' in chunk: print('*Directory ID:* ' + chunk['Directory ID'])
        if 'Active' in chunk: print('*Active:* ' + chunk['Active'])
        if 'Directory Type' in chunk: print('*Directory Type:* ' + chunk['Directory Type'])
        if 'crowd.delegated.directory.type' in chunk: print(
        '*Delegated Directory Type:* ' + chunk['crowd.delegated.directory.type'])
        if 'ldap.url' in chunk: print('*Hostname:* ' + chunk['ldap.url'])
        if 'crowd.server.url' in chunk: print('*Crowd Server URL:* ' + chunk['crowd.server.url'])
        if 'application.name' in chunk: print('*Application Name:* ' + chunk['application.name'])
        if 'user_encryption_method' in chunk: print('*Encryption Method:* ' + chunk['user_encryption_method'])
        if 'ldap.userdn' in chunk: print('*Username:* ' + chunk['ldap.userdn'])
        if 'ldap.password' in chunk: print('*Password:* ' + chunk['ldap.password'])
        if 'autoAddGroups' in chunk: print('*Default Group Memberships:* ' + chunk['autoAddGroups'])
        if 'crowd.delegated.directory.auto.create.user' in chunk: print('*Automatically Create User On Login:* ' + chunk['crowd.delegated.directory.auto.create.user'])
        if 'crowd.delegated.directory.auto.update.user' in chunk: print('*Automatically Update User On Login:* ' + chunk['crowd.delegated.directory.auto.update.user'])
        if 'crowd.delegated.directory.importGroups' in chunk: print(
        '*Synchronize Group Members:* ' + chunk['crowd.delegated.directory.importGroups'])

        if 'ldap.basedn' in chunk: print('\nh4. LDAP Schema\n')

        if 'ldap.basedn' in chunk: print('*Base DN:* ' + chunk['ldap.basedn'])
        if 'ldap.user.dn' in chunk: print('*Additional User DN:* ' + chunk['ldap.user.dn'])
        if 'ldap.group.dn' in chunk: print('*Additional Group DN:* ' + chunk['ldap.group.dn'])
        if 'ldap.local.groups' in chunk: print('*Read Only, with Local Groups:* ' + chunk['ldap.local.groups'])

        if 'com.atlassian.crowd.directory.sync.laststartsynctime' in chunk: print('\nh4. Synchronization Information\n')

        if 'com.atlassian.crowd.directory.sync.issynchronising' in chunk: print('*Currently Synchronizing:* ' + chunk['com.atlassian.crowd.directory.sync.issynchronising'])
        if 'com.atlassian.crowd.directory.sync.currentstartsynctime' in chunk: print(
        '*Current Sync Start Time:* ' + chunk['com.atlassian.crowd.directory.sync.currentstartsynctime'])
        if 'com.atlassian.crowd.directory.sync.laststartsynctime' in chunk: print('*Last Sync Start Time:* ' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(float(chunk['com.atlassian.crowd.directory.sync.laststartsynctime'])/1000)))
        if 'com.atlassian.crowd.directory.sync.lastdurationms' in chunk: print(
        '*Last Sync Duration (ms)*: ' + chunk['com.atlassian.crowd.directory.sync.lastdurationms'])

        print('\nh4. Advanced Settings\n')

        if 'ldap.nestedgroups.disabled' in chunk: print(
        '*Disable Nested Groups:* ' + chunk['ldap.nestedgroups.disabled'])
        if 'localUserStatusEnabled' in chunk: print('*Manage User Status Locally:* ' + chunk['localUserStatusEnabled'])
        if 'ldap.filter.expiredUsers' in chunk: print(
        '*Filter out expired Users:* ' + chunk['ldap.filter.expiredUsers'])
        if 'ldap.pagedresults' in chunk: print('*Use Paged Results:* ' + chunk['ldap.pagedresults'])
        if 'ldap.pagedresults.size' in chunk: print('*Number of Paged Results:* ' + chunk['ldap.pagedresults.size'])
        if 'ldap.referral' in chunk: print('*Follow Referrals:* ' + chunk['ldap.referral'])
        if 'ldap.relaxed.dn.standardisation' in chunk: print(
        '*Native DN Matching:* ' + chunk['ldap.relaxed.dn.standardisation'])
        if 'crowd.sync.incremental.enabled' in chunk: print(
        '*Incremental Synchronization:* ' + chunk['crowd.sync.incremental.enabled'])
        if 'crowd.sync.group.membership.after.successful.user.auth.enabled' in chunk: print(
        '*Update group memberships when logging in:* ' + chunk[
            'crowd.sync.group.membership.after.successful.user.auth.enabled'])
        if 'directory.cache.synchronise.interval' in chunk: print(
        '*Synchronization Interval:* ' + chunk['directory.cache.synchronise.interval'])
        if 'ldap.read.timeout' in chunk: print('*Read timeout:* ' + chunk['ldap.read.timeout'])
        if 'ldap.search.timelimit' in chunk: print('*Search Timeout:* ' + chunk['ldap.search.timelimit'])
        if 'ldap.connection.timeout' in chunk: print('*Connection Timeout:* ' + chunk['ldap.connection.timeout'])

        if 'ldap.user.filter' in chunk: print('\nh4. User Schema Settings\n')

        if 'ldap.user.objectclass' in chunk: print('*User Object Class:* ' + chunk['ldap.user.objectclass'])
        if 'ldap.user.filter' in chunk: print('*User Object Filter:* ' + chunk['ldap.user.filter'])
        if 'ldap.user.username' in chunk: print('*User Name Attribute:* ' + chunk['ldap.user.username'])
        if 'ldap.user.username.rdn' in chunk: print('*User Name RDN Attribute:* ' + chunk['ldap.user.username.rdn'])
        if 'ldap.user.firstname' in chunk: print('*User First Name Attribute:* ' + chunk['ldap.user.firstname'])
        if 'ldap.user.lastname' in chunk: print('*User Last Name Attribute:* ' + chunk['ldap.user.lastname'])
        if 'ldap.user.displayname' in chunk: print('*User Display Name Attribute:* ' + chunk['ldap.user.displayname'])
        if 'ldap.user.email' in chunk: print('*User Email Attribute:* ' + chunk['ldap.user.email'])
        if 'ldap.user.password' in chunk: print('*User Password Attribute:* ' + chunk['ldap.user.password'])
        if 'ldap.user.external.id' in chunk: print('*User Unique ID Attribute:* ' + chunk['ldap.user.external.id'])

        if 'ldap.group.filter' in chunk: print('\nh4. Group Schema Settings\n')

        if 'ldap.group.objectclass' in chunk: print('*Group Object Class:* ' + chunk['ldap.group.objectclass'])
        if 'ldap.group.filter' in chunk: print('*Group Object Filter:* ' + chunk['ldap.group.filter'])
        if 'ldap.group.name' in chunk: print('*Group Name Attribute:* ' + chunk['ldap.group.name'])
        if 'ldap.group.description' in chunk: print('*Group Description Attribute:* ' + chunk['ldap.group.description'])

        if 'ldap.user.group' in chunk: print('\nh4. Membership Schema Settings\n')

        if 'ldap.group.usernames' in chunk: print('*Group Members Attribute:* ' + chunk['ldap.group.usernames'])
        if 'ldap.user.group' in chunk: print('*User Membership Attribute:* ' + chunk['ldap.user.group'])
        if 'ldap.usermembership.use' in chunk: print(
        '*When finding the user\'s group membership:* ' + chunk['ldap.usermembership.use'])
        if 'ldap.usermembership.use.for.groups' in chunk: print(
        '*When finding the members of a group:* ' + chunk['ldap.usermembership.use.for.groups'])

        print('{panel}')

    def main(self):

        chunks = self.processor(content)

        if self.jira:
            for bog in chunks:
                if bog['Directory Type'] == 'INTERNAL':
                    continue
                else:
                    self.jira_printer(bog)
        else:
            print('\n-------\n')
            for bog in chunks:
                if bog['Directory Type'] == 'INTERNAL':
                    continue
                else:
                    self.text_printer(bog)
                    print('\n-------\n')


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "input_file",
        help="The directoryConfigurationSummary.txt you want to process.")

    parser.add_argument(
        "-j",
        "--jira",
        action='store_true',
        help="Use this flag for Jira markup output.")

    options = parser.parse_args()

    # Splitting the file up into a list for easier processing.
    with open(options.input_file) as f:
        content = f.read().splitlines()

    bog = BetterUserDirectory(
        content=content,
        jira=options.jira
        )

    bog.main()